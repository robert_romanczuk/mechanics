// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Units/Mech.h"


// Sets default values
AMech::AMech()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	CapsuleComponent = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleComponent"));
	CapsuleComponent->SetupAttachment(RootComponent);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(RootComponent);

	this->HP = 10;
	this->MaxSpeed = 4;
	this->ActionsDistanceMax = 3;
	this->ActionsDistanceLeft = 3;
}

// Called when the game starts or when spawned
void AMech::BeginPlay()
{
	Super::BeginPlay();

	MoveInTurn = false;
}

// Called every frame
void AMech::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!MoveInTurn)
	{
		if (Path.IsValidIndex(0))
		{
			MoveDestination = Path[0];
			if (IsValid(MoveDestination->GetTile()))
			{
				MoveInTurn = true;
				Path.RemoveAt(0);
			}
		}
	}

	if (MoveInTurn)
	{
		FVector ToActorLocation = MoveDestination->GetTile()->GetActorLocation();
		FVector FromActorLocation = this->GetActorLocation();

		FVector Distance = ToActorLocation - FromActorLocation;

		if ((FMath::Abs(Distance.X) <= MaxSpeed) && (FMath::Abs(Distance.Y) <= MaxSpeed))
		{
			this->SetActorLocation(ToActorLocation);
			MoveInTurn = false;
		} else
		{
			int deltaX = Distance.X > 0 ? MaxSpeed : -MaxSpeed;
			int deltaY = Distance.Y > 0 ? MaxSpeed : -MaxSpeed;

			if (FromActorLocation.X != ToActorLocation.X)
			{
				FromActorLocation.X += deltaX;
			}

			if (FromActorLocation.Y != ToActorLocation.Y)
			{
				FromActorLocation.Y += deltaY;
			}
		}

		this->SetActorLocation(FromActorLocation);
	}
}

// Called to bind functionality to input
void AMech::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void AMech::SetStaticMesh(UStaticMesh * StaticMesh)
{
	this->StaticMeshComponent->SetStaticMesh(StaticMesh);
}

//APath * AMech::GetPath()
TArray<APlaneNode*> AMech::GetPath()
{
	return this->Path;
}

//void AMech::SetPath(APath * NewPath)
void AMech::SetPath(TArray<APlaneNode*> NewPath)
{
	this->Path = NewPath;
}

void AMech::Move(AActor * MoveToActor)
{
	APlaneNode* MoveTo = Cast<APlaneNode>(MoveToActor);
	MoveTo->SetActorOnTile(this, false);
}

bool AMech::GetMoveInTurn()
{
	return MoveInTurn;
}

int32 AMech::GetActionsDistanceMax()
{
	return this->ActionsDistanceMax;
}

int32 AMech::GetActionsDistanceLeft()
{
	return this->ActionsDistanceLeft;
}

void AMech::SetActionsDistanceLeft(int32 Left)
{
	this->ActionsDistanceLeft = Left;
}

void AMech::SetTeam(ETeam NewTeam)
{
	this->Team = NewTeam;
}

ETeam AMech::GetTeam()
{
	return this->Team;
}

void AMech::SetHP(float NewHP)
{
	this->HP = NewHP;
}

float AMech::GetHP()
{
	return this->HP;
}

UOrder* AMech::OrderMove(AActor * MoveToActor)
{
	UOrder* Order = NewObject<UOrder>();
	Order->MoveOrder(EOrderTypes::MoveToActor, this, MoveToActor);
	return Order;
}
