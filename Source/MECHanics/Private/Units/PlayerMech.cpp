// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Units/PlayerMech.h"


// Sets default values
APlayerMech::APlayerMech()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	this->StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
}

// Called when the game starts or when spawned
void APlayerMech::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APlayerMech::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void APlayerMech::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}

void APlayerMech::SetStaticMesh(UStaticMesh * StaticMesh)
{
	this->StaticMeshComponent->SetStaticMesh(StaticMesh);
}