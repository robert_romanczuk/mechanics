// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Core/Pathfinder.h"

void UPathfinder::AStar(APlaneNode* StartNode, APlaneNode* EndNode)
{
	TArray<APlaneNode*> ClosedSet = TArray<APlaneNode*>();
	TArray<APlaneNode*> OpenSet = TArray<APlaneNode*>({ StartNode });

	
}

TArray<APlaneNode*> UPathfinder::BreadthFirstSearch(AHexGrid* HexGrid, APlaneNode* StartNode, APlaneNode* EndNode, bool AI)
{
	TArray<APlaneNode*> Path;
	TArray<APlaneNode*> Neighbours;

	if (IsValid(StartNode) && IsValid(EndNode))
	{
		TQueue<APlaneNode*> Frontier;

		Frontier.Enqueue(StartNode);

		TMap<APlaneNode*, APlaneNode*> CameFrom = TMap<APlaneNode*, APlaneNode*>();
		CameFrom.Add(StartNode, nullptr);

		APlaneNode* Current;
		while (!Frontier.IsEmpty())
		{
			Frontier.Dequeue(Current);

			if (AI)
			{
				Neighbours = HexGrid->GetNeighbours(Current, EndNode, true);
			} else
			{
				Neighbours = HexGrid->GetNeighbours(Current);
			}

			for (APlaneNode* Next : Neighbours)
			{
				if (!CameFrom.Contains(Next))
				{
					Frontier.Enqueue(Next);
					CameFrom.Add(Next, Current);
				}
			}
		}

		// Reconstruct path from goal to start
		Current = EndNode;

		TArray<APlaneNode*> ReversedPath = TArray<APlaneNode*>();

		ReversedPath.Add(Current);

		while (Current != StartNode)
		{
			if (!CameFrom.Contains(Current))
			{
				break;
			}
			Current = *CameFrom.Find(Current);
			ReversedPath.Add(Current);
		}

		for (size_t i = 0; i < ReversedPath.Num(); i++)
		{
			Path.Add(ReversedPath[ReversedPath.Num() - 1 - i]);
		}
	}

	return Path;
}


