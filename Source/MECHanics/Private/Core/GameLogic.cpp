// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Core/GameLogic.h"


// Sets default values
AGameLogic::AGameLogic()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGameLogic::BeginPlay()
{
	Super::BeginPlay();

	MainOrdersSet = NewObject<UMainOrdersSet>();
	MainOrdersSet->Init();

	IsPlayerTurn = true;
}

// Called every frame
void AGameLogic::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AGameLogic::SelectMech(AMech * Mech)
{
	this->SelectedMech = Mech;
}

AMech * AGameLogic::GetSelectedMech()
{
	return this->SelectedMech;
}

UMainOrdersSet * AGameLogic::GetMainOrdersSet()
{
	return MainOrdersSet;
}

void AGameLogic::SetPlayerTurn(bool PlayerTurn)
{
	this->IsPlayerTurn = PlayerTurn;
}

bool AGameLogic::GetPlayerTurn()
{
	return this->IsPlayerTurn;
}

