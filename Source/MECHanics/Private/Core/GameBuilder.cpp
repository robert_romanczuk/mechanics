// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Core/GameBuilder.h"


// Sets default values
AGameBuilder::AGameBuilder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AGameBuilder::BeginPlay()
{
	Super::BeginPlay();

	AHexGrid* HexGrid = GetWorld()->SpawnActor<AHexGrid>(HexGridBlueprint);
	HexGrid->SetGridSize(this->GridSizeX, this->GridSizeY);
	HexGrid->SetStaticMeshes(this->GridStaticMeshes);
	HexGrid->SetHexBlueprint(this->HexBlueprint);
	HexGrid->SetObstacleBlueprint(this->ObstacleBlueprint);
	HexGrid->CreateGrid();

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	AMech* Mech = GetWorld()->SpawnActor<AMech>(MechBlueprint, SpawnParameters);
	Mech->SetStaticMesh(this->UnitStaticMesh);

	APlaneNode* MechHex = HexGrid->GetGrid()[0];

	MechHex->SetActorOnTile(Mech);
	Mech->SetActorLocation(MechHex->GetTile()->GetActorLocation());
	Mech->SetTeam(ETeam::FRIENDLY);

	AMech* Mech2 = GetWorld()->SpawnActor<AMech>(MechBlueprint, SpawnParameters);
	Mech2->SetStaticMesh(this->UnitStaticMesh);
	
	APlaneNode* Mech2Hex = HexGrid->GetGrid()[1];

	Mech2Hex->SetActorOnTile(Mech2);
	Mech2->SetActorLocation(Mech2Hex->GetTile()->GetActorLocation());
	Mech2->SetTeam(ETeam::FRIENDLY);

	int GridSize = HexGrid->GetGrid().Num();

	AMech* Mech3 = GetWorld()->SpawnActor<AMech>(MechBlueprint, SpawnParameters);
	Mech3->SetStaticMesh(this->UnitStaticMesh);

	AAIController* Mech3AIController = GetWorld()->SpawnActor<AAIController>(AIControllerBlueprint, SpawnParameters);
	Mech3AIController->Possess(Mech3);

	APlaneNode* Mech3Hex = HexGrid->GetGrid()[GridSize - 1];

	Mech3Hex->SetActorOnTile(Mech3);
	Mech3->SetActorLocation(Mech3Hex->GetTile()->GetActorLocation());
	Mech3->SetTeam(ETeam::ENEMY);

	AMech* Mech4 = GetWorld()->SpawnActor<AMech>(MechBlueprint, SpawnParameters);
	Mech4->SetStaticMesh(this->UnitStaticMesh);

	AAIController* Mech4AIController = GetWorld()->SpawnActor<AAIController>(AIControllerBlueprint, SpawnParameters);
	Mech4AIController->Possess(Mech4);

	APlaneNode* Mech4Hex = HexGrid->GetGrid()[GridSize - 2];

	Mech4Hex->SetActorOnTile(Mech4);
	Mech4->SetActorLocation(Mech4Hex->GetTile()->GetActorLocation());
	Mech4->SetTeam(ETeam::ENEMY);
}

// Called every frame
void AGameBuilder::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

