// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "MECHanics.h"
#include "../Public/Core/MainOrdersSet.h"

void UMainOrdersSet::Init()
{
	OrdersSet = TSet<UOrder*>();
}

void UMainOrdersSet::Clear()
{
	OrdersSet.Empty(0);
}

void UMainOrdersSet::AddOrder(UOrder * Order)
{
	OrdersSet.Add(Order);
}

TArray<UOrder*> UMainOrdersSet::GetAll()
{
	return OrdersSet.Array();
}
