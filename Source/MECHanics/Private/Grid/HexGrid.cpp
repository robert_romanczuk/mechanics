﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Grid/HexGrid.h"


// Sets default values
AHexGrid::AHexGrid()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AHexGrid::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AHexGrid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

//Creates hex grid
void AHexGrid::CreateGrid()
{
	FVector Origin;
	FVector BoundsExtent;
	double Width;
	double Height;
	double DistanceHorizontal;
	double DistanceVertical;
	FVector Offset = FVector(0, 0, 0);
	bool BoundsTaken = false;

	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	for (int32 x = 0; x < this->GridSizeX; x++)
	{
		for (int32 y = 0; y < this->GridSizeY; y++)
		{
			APlaneNode* PlaneNode = GetWorld()->SpawnActor<APlaneNode>(SpawnParameters);			
			PlaneNode->SetXY(x, y);

			AHex* Spawned = GetWorld()->SpawnActor<AHex>(HexBlueprint, SpawnParameters);
			Spawned->SetStaticMesh(GetRandomMesh());

			PlaneNode->SetTile(Spawned);
			PlaneNode->SetLocation(Offset);

			if (!BoundsTaken)
			{
				BoundsExtent = Spawned->StaticMeshComponent->Bounds.BoxExtent;
				Origin = Spawned->StaticMeshComponent->Bounds.Origin;

				Width = BoundsExtent.X * 2;
				DistanceHorizontal = Width * 0.75f;

				Height = (sqrt(3) / 2) * Width;
				DistanceVertical = Height / 2;

				BoundsTaken = true;
			} else
			{
				FVector Location;
				if (y % 2 == 0)
				{
					Location = FVector(Offset.X + (DistanceHorizontal * y), Offset.Y, Offset.Z);
				} else
				{
					Location = FVector(Offset.X + (DistanceHorizontal * y), Offset.Y + DistanceVertical, Offset.Z);
				}
				PlaneNode->SetLocation(Location);

				// Obstacle spawn
				if (FMath::RandRange(0, 100) <= 5) //5%
				{
					if (!PlaneNode->CheckIfOccupied())
					{
						PutObstacle(PlaneNode, ObstacleBlueprint.GetDefaultObject());
					}
				}
			}

			Grid.Add(PlaneNode);
		}
		Offset.Y = Origin.Y + Height;
		Offset.X = 0;
		BoundsTaken = false;
	}
}

void AHexGrid::SetGridSize(int32 SizeX, int32 SizeY)
{
	this->GridSizeX = SizeX;
	this->GridSizeY = SizeY;
}

void AHexGrid::SetStaticMeshes(TArray<UStaticMesh*> StaticMeshes)
{
	this->GridStaticMeshes = StaticMeshes;
}

void AHexGrid::SetHexBlueprint(TSubclassOf<class AHex> Blueprint)
{
	this->HexBlueprint = Blueprint;
}

void AHexGrid::SetObstacleBlueprint(TSubclassOf<class AActor> Blueprint)
{
	this->ObstacleBlueprint = Blueprint;
}

TArray<APlaneNode*> AHexGrid::GetGrid()
{
	return this->Grid;
}

TArray<APlaneNode*> AHexGrid::GetNeighbours(APlaneNode * Node, APlaneNode* EndNode, bool AppendOccupiedEndNode)
{
	TArray<APlaneNode*> Neighbours = TArray<APlaneNode*>();
	for (APlaneNode* GridHex : Grid)
	{
		int distanceX = abs(Node->GetX() - GridHex->GetX());
		int distanceY = abs(Node->GetY() - GridHex->GetY());
		
		if ((distanceX == 1 && distanceY == 0) ||
			(distanceY == 1 && distanceX == 0) ||
			((Node->GetY() % 2 == 0) && (
				((Node->GetY() + 1 == GridHex->GetY()) && (Node->GetX() - 1 == GridHex->GetX())) || ((Node->GetY() - 1 == GridHex->GetY()) && (Node->GetX() - 1 == GridHex->GetX()))
				)) ||
			((Node->GetY() % 2 != 0) && (
				((Node->GetY() - 1 == GridHex->GetY()) && (Node->GetX() + 1 == GridHex->GetX())) || ((Node->GetY() + 1 == GridHex->GetY()) && (Node->GetX() + 1 == GridHex->GetX()))
				))
			)
		{
			if (AppendOccupiedEndNode && (GridHex == EndNode))
			{
				Neighbours.Add(GridHex);
			}
			else if (!GridHex->CheckIfOccupied())
			{
				Neighbours.Add(GridHex);
			}
		}
	}
	return Neighbours;
}

void AHexGrid::PutObstacle(APlaneNode* PutOn, AActor * Obstacle)
{
	AActor* SpawnedObstacle = GetWorld()->SpawnActor<AActor>(Obstacle->GetClass());
	PutOn->SetActorOnTile(SpawnedObstacle);
}

UStaticMesh * AHexGrid::GetRandomMesh()
{
	return this->GridStaticMeshes[0];
}

int32 AHexGrid::CalculateDistance(APlaneNode * FromNode, APlaneNode * ToNode)
{
	int32 distance = 0;

	if (IsValid(FromNode) && IsValid(ToNode))
	{
		HexToCube(FromNode);
		HexToCube(ToNode);

		distance = FMath::Max3(abs(FromNode->GetX() - ToNode->GetX()), abs(FromNode->GetY() - ToNode->GetY()), abs(FromNode->GetZ() - ToNode->GetZ()));

		CubeToHex(FromNode);
		CubeToHex(ToNode);
	}

	return distance;
}

TArray<APlaneNode*> AHexGrid::MovementRange(int32 Range, APlaneNode* FromNode)
{
	TArray<APlaneNode*> Visited;

	if (IsValid(FromNode))
	{
		Visited.Add(FromNode);

		TArray<TArray<APlaneNode*>> Reachable;
		Reachable.Add(TArray<APlaneNode*>({ FromNode }));

		for (size_t k = 1; k <= Range; k++)
		{
			Reachable.Add(TArray<APlaneNode*>());
			for (auto& Node : Reachable[k - 1])
			{
				for (size_t i = 0; i < 6; i++)
				{
					for (auto& Neighbour : GetNeighbours(Node))
					{
						if (!Visited.Contains(Neighbour))
						{
							Visited.Add(Neighbour);
							Reachable[k].Add(Neighbour);
						}
					}
				}
			}
		}
	}

	return Visited;
}

void AHexGrid::CubeToHex(APlaneNode * Node)
{
	// odd - q offset
	//Node->SetY(Node->GetZ() + (Node->GetX() - (Node->GetX() & 1)) / 2);
	//Node->SetX(Node->GetX());

	// odd-r offset
	Node->SetX(Node->GetX() + (Node->GetZ() - (Node->GetZ() & 1)) / 2);
	Node->SetY(Node->GetZ());
}

void AHexGrid::HexToCube(APlaneNode * Node)
{
	// odd - q offset
	//Node->SetX(Node->GetX());
	//Node->SetZ(Node->GetY() - (Node->GetX() - (Node->GetX() & 1)) / 2);
	//Node->SetY(-Node->GetX() - Node->GetZ());

	// odd - r offset
	Node->SetX(Node->GetX() - (Node->GetY() - (Node->GetY() & 1)) / 2);
	Node->SetZ(Node->GetY());
	Node->SetY(-Node->GetX() - Node->GetZ());
}