// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "PlaneNode.h"

// Sets default values
APlaneNode::APlaneNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlaneNode::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlaneNode::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void APlaneNode::SetTile(AHex * TileActor)
{
	this->Tile = TileActor;
	this->Tile->SetActorLocation(this->GetActorLocation());
}

AHex * APlaneNode::GetTile()
{
	return this->Tile;
}

bool APlaneNode::CheckIfOccupied()
{
	return this->IsOccupied;
}

void APlaneNode::SetActorOnTile(AActor* Actor, bool Move)
{
	this->ActorOnTile = Actor;

	if (Move)
	{
		this->ActorOnTile->SetActorLocation(this->GetTile()->GetActorLocation());
	}

	this->IsOccupied = true;
}

AActor * APlaneNode::GetActorOnTile()
{
	return this->ActorOnTile;
}

void APlaneNode::ClearActorOnTile()
{
	this->ActorOnTile = nullptr;
	this->IsOccupied = false;
}

void APlaneNode::SetX(int32 X)
{
	this->X = X;
}

void APlaneNode::SetY(int32 Y)
{
	this->Y = Y;
}

void APlaneNode::SetZ(int32 Z)
{
	this->Z = Z;
}

int32 APlaneNode::GetX()
{
	return this->X;
}

int32 APlaneNode::GetY()
{
	return this->Y;
}

int32 APlaneNode::GetZ()
{
	return this->Z;
}

void APlaneNode::SetXY(int32 X, int32 Y)
{
	this->X = X;
	this->Y = Y;
}

void APlaneNode::SetLocation(FVector NewLocation)
{
	this->SetActorLocation(NewLocation);

	if (IsValid(this->Tile))
	{
		this->Tile->SetActorLocation(NewLocation);
	}

	if (IsValid(this->ActorOnTile))
	{
		this->ActorOnTile->SetActorLocation(NewLocation);
	}
}

FVector APlaneNode::GetLocation()
{
	if (IsValid(this->Tile))
	{
		return this->Tile->GetActorLocation();
	} else
	{
		return this->GetActorLocation();
	}
}
