// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Grid/Hex.h"


// Sets default values
AHex::AHex()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));
	BoxComponent->SetupAttachment(RootComponent);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(RootComponent);

	ParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));
	ParticleSystemComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned 
void AHex::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHex::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AHex::SetStaticMesh(UStaticMesh * Mesh)
{
	this->StaticMeshComponent->SetStaticMesh(Mesh);
}

bool AHex::IsOccupied()
{
	return Ocuppied;
}

void AHex::SetOcuppyingActor(AActor * Actor)
{
	if (Ocuppied)
	{
		ClearOcuppyingActor();
	}
	OcuppyingActor = Actor;
	Ocuppied = true;
}

AActor * AHex::GetOcuppyingActor()
{
	return OcuppyingActor;
}

void AHex::ClearOcuppyingActor()
{
	if (Ocuppied)
	{
		Ocuppied = false;
	}
}
