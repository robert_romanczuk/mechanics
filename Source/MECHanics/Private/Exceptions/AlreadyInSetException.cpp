// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Exceptions/AlreadyInSetException.h"


AlreadyInSetException::AlreadyInSetException()
{
}

AlreadyInSetException::~AlreadyInSetException()
{
}

AlreadyInSetException::AlreadyInSetException(const char * Msg) : Msg(Msg)
{
}

const char * AlreadyInSetException::what() const throw()
{
	return Msg;
}
