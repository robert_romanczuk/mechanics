// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Orders/OrdersSet.h"

void UOrdersSet::AddOrder(UOrder * Order)
{
	bool AlreadyInSet;
	Orders.Add(Order, &AlreadyInSet);

	if (AlreadyInSet)
	{
		throw AlreadyInSetException("Order already in orders set!");
	}
}

TArray<UOrder*> UOrdersSet::GetAll()
{
	return Orders.Array();
}



