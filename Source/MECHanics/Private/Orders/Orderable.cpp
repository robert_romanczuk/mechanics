// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Orders/Orderable.h"


// This function does not need to be modified.
UOrderable::UOrderable(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any IOrderable functions that are not pure virtual.
