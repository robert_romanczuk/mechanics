// Fill out your copyright notice in the Description page of Project Settings.

#include "MECHanics.h"
#include "../Public/Orders/Order.h"

void UOrder::MoveOrder(EOrderTypes Type, AActor* FromActor, AActor* ToActor)
{
	Type = EOrderTypes::MoveToActor;
	this->FromActor = FromActor;
	this->ToActor = ToActor;
}

EOrderTypes UOrder::GetType()
{
	return Type;
}

AActor* UOrder::GetToActor()
{
	return ToActor;
}

AActor* UOrder::GetFromActor()
{
	return FromActor;
}

//void UOrder::MoveToActor()
//{
//	FVector ToActorLocation = ToActor->GetActorLocation();
//	FVector FromActorLocation = FromActor->GetActorLocation();
//
//	FVector Distance = ToActorLocation - FromActorLocation;
//
//
//	int deltaX = Distance.X > 0 ? 1 : -1;
//	int deltaY = Distance.Y > 0 ? 1 : -1;
//
//	while ((Distance.X >= 1) || (Distance.Y >= 1))
//	{
//		if (FromActorLocation.X != ToActorLocation.X)
//		{
//			FromActorLocation.X += deltaX;
//		}
//
//		if (FromActorLocation.Y != ToActorLocation.Y)
//		{
//			FromActorLocation.Y += deltaY;
//		}
//				
//		FromActor->SetActorLocation(FromActorLocation);
//		Distance = ToActorLocation - FromActorLocation;
//	}
//
//	FromActor->SetActorLocation(ToActorLocation);
//}