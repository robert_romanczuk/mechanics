// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Grid.generated.h"

/**
 * 
 */
UCLASS()
class MECHANICS_API UGrid : public UObject
{
	GENERATED_BODY()
	
private:
	//TArray<UGridNode*> Grid;

	int32 GridSizeX;
	int32 GridSizeY;

public:
	/*UFUNCTION(BlueprintCallable, Category = "Grid")
		TArray<UGridNode*> GetGridAsArray();*/

	////Creates grid
	//UFUNCTION(BlueprintCallable, Category = "Grid")
	//	void CreateGrid(int32 GridSizeX, int32 GridSizeY);
};
