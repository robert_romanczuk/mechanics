// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Hex.generated.h"

UCLASS()
class MECHANICS_API AHex : public AActor
{
	GENERATED_BODY()

private:
	bool Ocuppied;
	AActor* OcuppyingActor;

public:
	// Sets default values for this actor's properties
	AHex();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere)
		UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StaticMeshComponent;

	UPROPERTY(EditAnywhere)
		UParticleSystemComponent* ParticleSystemComponent;

	UFUNCTION()
		void SetStaticMesh(UStaticMesh* Mesh);

	UFUNCTION(BlueprintCallable, Category = "Hex")
		bool IsOccupied();

	UFUNCTION(BlueprintCallable, Category = "Hex")
		void SetOcuppyingActor(AActor* Actor);

	UFUNCTION(BlueprintCallable, Category = "Hex")
		AActor* GetOcuppyingActor();

	UFUNCTION(BlueprintCallable, Category = "Hex")
		void ClearOcuppyingActor();
};
