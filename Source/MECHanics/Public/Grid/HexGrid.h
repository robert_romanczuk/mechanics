// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Hex.h"
#include "PlaneNode.h"
#include "HexGrid.generated.h"

UCLASS()
class MECHANICS_API AHexGrid : public AActor
{
	GENERATED_BODY()

private:
	TArray<APlaneNode*> Grid;

	UFUNCTION()
		UStaticMesh* GetRandomMesh();

	UFUNCTION()
		void CubeToHex(APlaneNode* Node);

	UFUNCTION()
		void HexToCube(APlaneNode* Node);

public:
	// Sets default values for this actor's properties
	AHexGrid();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(EditAnywhere)
		int32 GridSizeX;

	UPROPERTY(EditAnywhere)
		int32 GridSizeY;

	UPROPERTY(EditAnywhere)
		TArray<UStaticMesh*> GridStaticMeshes;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AHex> HexBlueprint;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class AActor> ObstacleBlueprint;

	UFUNCTION(BlueprintCallable, Category = "Grid")
		void CreateGrid();

	UFUNCTION(BlueprintCallable, Category = "Grid")
		void SetGridSize(int32 SizeX, int32 SizeY);

	UFUNCTION(BlueprintCallable, Category = "Grid")
		void SetStaticMeshes(TArray<UStaticMesh*> StaticMeshes);

	UFUNCTION(BlueprintCallable, Category = "Grid")
		void SetHexBlueprint(TSubclassOf<class AHex> Blueprint);

	UFUNCTION(BlueprintCallable, Category = "Grid")
		void SetObstacleBlueprint(TSubclassOf<class AActor> Blueprint);

	UFUNCTION(BlueprintCallable, Category = "Grid")
		TArray<APlaneNode*> GetGrid();

	UFUNCTION(BlueprintCallable, Category = "Grid")
		TArray<APlaneNode*> GetNeighbours(APlaneNode* Node, APlaneNode* EndNode = nullptr, bool AppendOccupiedEndNode = false);

	UFUNCTION(BlueprintCallable, Category = "Grid")
		void PutObstacle(APlaneNode* PutOn, AActor* Obstacle);

	UFUNCTION(BlueprintCallable, Category = "Grid")
		int32 CalculateDistance(APlaneNode * FromNode, APlaneNode * ToNode);

	UFUNCTION(BlueprintCallable, Category = "Grid")
		TArray<APlaneNode*> MovementRange(int32 Range, APlaneNode* FromNode);

};
