// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "PathNode.h"
#include "Path.generated.h"

UCLASS()
class MECHANICS_API APath : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APath();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	TDoubleLinkedList<APathNode*> Path;

	UFUNCTION(BlueprintCallable, Category = "Functions")
		void AddPathNode(APathNode* const PathNode);

	UFUNCTION(BlueprintCallable, Category = "Functions")
		APathNode* GetFirstNode();
};
