// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Hex.h"
#include "PlaneNode.generated.h"

UCLASS()
class MECHANICS_API APlaneNode : public AActor
{
	GENERATED_BODY()

private:
	UPROPERTY()
		AHex* Tile;

	UPROPERTY()
		AActor* ActorOnTile;

	UPROPERTY()
		bool IsOccupied;

	int32 X;
	int32 Y;
	int32 Z;
public:
	// Sets default values for this actor's properties
	APlaneNode();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Node
	UFUNCTION(BlueprintCallable, Category = "Node")
		void SetX(int32 X);

	UFUNCTION(BlueprintCallable, Category = "Node")
		void SetY(int32 Y);

	UFUNCTION(BlueprintCallable, Category = "Node")
		void SetZ(int32 Z);

	UFUNCTION(BlueprintCallable, Category = "Node")
		int32 GetX();

	UFUNCTION(BlueprintCallable, Category = "Node")
		int32 GetY();

	UFUNCTION(BlueprintCallable, Category = "Node")
		int32 GetZ();

	UFUNCTION(BlueprintCallable, Category = "Node")
		void SetXY(int32 X, int32 Y);

	UFUNCTION(BlueprintCallable, Category = "Node")
		void SetLocation(FVector NewLocation);

	UFUNCTION(BlueprintCallable, Category = "Node")
		FVector GetLocation();

	// Tile
	UFUNCTION(BlueprintCallable, Category = "Tile")
		void SetTile(AHex* TileActor);

	UFUNCTION(BlueprintCallable, Category = "Tile")
		AHex* GetTile();

	UFUNCTION(BlueprintCallable, Category = "Tile")
		bool CheckIfOccupied();

	// Unit
	UFUNCTION(BlueprintCallable, Category = "Unit")
		void SetActorOnTile(AActor* Actor, bool Move = true);

	UFUNCTION(BlueprintCallable, Category = "Unit")
		AActor* GetActorOnTile();

	UFUNCTION(BlueprintCallable, Category = "Unit")
		void ClearActorOnTile();
};
