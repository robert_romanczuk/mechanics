// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Grid/Hex.h"
#include "../Grid/HexGrid.h"
#include "../Grid/PlaneNode.h"
#include "Pathfinder.generated.h"

/**
 * 
 */
UCLASS()
class MECHANICS_API UPathfinder : public UObject
{
	GENERATED_BODY()

public:
	static void AStar(APlaneNode* StartNode, APlaneNode* EndNode);

	UFUNCTION(BlueprintCallable, Category = "Pathfinding")
	static TArray<APlaneNode*> BreadthFirstSearch(AHexGrid* HexGrid, APlaneNode* StartNode, APlaneNode* EndNode, bool AI = false);


};
