// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MainOrdersSet.h"
#include "../Units/Mech.h"
#include "GameLogic.generated.h"

UCLASS()
class MECHANICS_API AGameLogic : public AActor
{
	GENERATED_BODY()

private:
	TArray<AActor*> Enemies;
	TArray<AActor*> Friendlies;

public:
	// Sets default values for this actor's properties
	AGameLogic();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Orders set (mech move, fire etc.)
	UPROPERTY(EditAnywhere, Category = "Actions")
		UMainOrdersSet* MainOrdersSet;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Units")
		AMech* SelectedMech;
	
	UFUNCTION(BlueprintCallable, Category = "Units")
		void SelectMech(AMech* Mech);

	UFUNCTION(BlueprintCallable, Category = "Units")
		AMech* GetSelectedMech();

	UFUNCTION(BlueprintCallable, Category = "Actions")
		UMainOrdersSet* GetMainOrdersSet();

	UPROPERTY(EditAnywhere, Category = "Actions")
		bool IsPlayerTurn;

	UFUNCTION(BlueprintCallable, Category = "Actions")
		void SetPlayerTurn(bool PlayerTurn);

	UFUNCTION(BlueprintCallable, Category = "Actions")
		bool GetPlayerTurn();
};
