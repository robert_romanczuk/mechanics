// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "GameLogic.h"
#include "../Grid/HexGrid.h"
#include "../Grid/PlaneNode.h"
#include "../Units/PlayerMech.h"
#include "../Units/Mech.h"
#include "../Controllers/MechAIController.h"
#include "GameBuilder.generated.h"

UCLASS()
class MECHANICS_API AGameBuilder : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGameBuilder();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UPROPERTY(EditAnywhere, Category = "Grid")
		TSubclassOf<class AHexGrid> HexGridBlueprint;

	UPROPERTY(EditAnywhere, Category = "Grid")
		TSubclassOf<class AHex> HexBlueprint;

	UPROPERTY(EditAnywhere, Category = "Grid")
		TSubclassOf<class AActor> ObstacleBlueprint;

	UPROPERTY(EditAnywhere, Category = "Grid")
		TArray<UStaticMesh*> GridStaticMeshes;

	UPROPERTY(EditAnywhere, Category = "Grid")
		uint16 GridSizeX;

	UPROPERTY(EditAnywhere, Category = "Grid")
		uint16 GridSizeY;

	UPROPERTY(EditAnywhere, Category = "Units")
		TSubclassOf<class AMech> MechBlueprint;

	UPROPERTY(EditAnywhere, Category = "Units")
		UStaticMesh* UnitStaticMesh;

	UPROPERTY(EditAnywhere, Category = "Units")
		TSubclassOf<class AMechAIController> AIControllerBlueprint;
};
