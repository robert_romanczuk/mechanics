// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Orders/Orderable.h"
#include "../Orders/Order.h"
#include "../Exceptions/AlreadyInSetException.h"
#include "MainOrdersSet.generated.h"

/**
 * 
 */
UCLASS()
class MECHANICS_API UMainOrdersSet : public UObject
{
	GENERATED_BODY()
	
private:
	TSet<UOrder*> OrdersSet;
	
public:
	void Init();

	UFUNCTION(BlueprintCallable, Category = "Orders")
		void Clear();

	UFUNCTION(BlueprintCallable, Category = "Orders")
		void AddOrder(UOrder* Order);

	UFUNCTION(BlueprintCallable, Category = "Orders")
		TArray<UOrder*> GetAll();
};
