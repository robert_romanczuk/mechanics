// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <iostream>
#include <exception>
/**
 * 
 */
class MECHANICS_API AlreadyInSetException
{
private:
	const char* Msg;

public:
	AlreadyInSetException();
	~AlreadyInSetException();

	AlreadyInSetException(const char* Msg);

	const char* what() const throw();
};