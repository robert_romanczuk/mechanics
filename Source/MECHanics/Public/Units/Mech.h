// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "../Grid/Path.h"
#include "../Orders/Orderable.h"
#include "../Grid/PlaneNode.h"
#include "Mech.generated.h"

UENUM(BlueprintType)
enum class ETeam : uint8
{
	FRIENDLY	UMETA(DisplayName = "Friendly"),
	ENEMY		UMETA(DisplayName = "Enemy")
};

UCLASS()
class MECHANICS_API AMech : public APawn, public IOrderable
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMech();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UPROPERTY(EditAnywhere)
		UStaticMeshComponent* StaticMeshComponent;

	UFUNCTION()
		void SetStaticMesh(UStaticMesh* StaticMesh);

	UPROPERTY(EditAnywhere)
		UCapsuleComponent* CapsuleComponent;

	UPROPERTY(EditAnywhere)
		//APath* Path;
		TArray<APlaneNode*> Path;

	UPROPERTY(EditAnywhere)
		APlaneNode* MoveDestination;

	UPROPERTY(EditAnywhere)
		bool MoveInTurn;

	/* Mech core parameters */
	UPROPERTY(EditAnywhere, Category = "Move")
		float MaxSpeed;

	UPROPERTY(EditAnywhere, Category = "Move")
		float Acceleration;

	UPROPERTY(EditAnywhere, Category = "Move")
		int32 ActionsDistanceMax;

	UPROPERTY(EditAnywhere, Category = "Move")
		int32 ActionsDistanceLeft;

	UPROPERTY(EditAnywhere, Category = "Hull")
		float HP;

	UPROPERTY(EditAnywhere, Category = "Damage")
		float Damage;
	
	UFUNCTION(BlueprintCallable, Category = "Path")
		//APath* GetPath();
		TArray<APlaneNode*> GetPath();

	UFUNCTION(BlueprintCallable, Category = "Path")
		//void SetPath(APath* NewPath);
		void SetPath(TArray<APlaneNode*> NewPath);

	UFUNCTION(BlueprintCallable, Category = "Order")
		UOrder* OrderMove(AActor* MoveToActor);

	UFUNCTION(BlueprintCallable, Category = "Move")
		void Move(AActor* MoveToActor);

	UFUNCTION(BlueprintCallable, Category = "Move")
		bool GetMoveInTurn();

	UFUNCTION(BlueprintCallable, Category = "Move")
		int32 GetActionsDistanceMax();

	UFUNCTION(BlueprintCallable, Category = "Move")
		int32 GetActionsDistanceLeft();

	UFUNCTION(BlueprintCallable, Category = "Move")
		void SetActionsDistanceLeft(int32 Left);

	UPROPERTY(EditAnywhere, Category = "State")
		ETeam Team;

	UFUNCTION(BlueprintCallable, Category = "State")
		void SetTeam(ETeam NewTeam);

	UFUNCTION(BlueprintCallable, Category = "State")
		ETeam GetTeam();

	UFUNCTION(BlueprintCallable, Category = "Hull")
		void SetHP(float NewHP);

	UFUNCTION(BlueprintCallable, Category = "Hull")
		float GetHP();
};
