// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Order.h"
#include "MoveToActorOrder.generated.h"

/**
 * 
 */
UCLASS()
class MECHANICS_API UMoveToActorOrder : public UOrder
{
	GENERATED_BODY()
	
public:
	AActor* FromActor;
	const AActor* ToActor;
	
	UMoveToActorOrder();
	//UMoveToActorOrder(AActor* FromActor, const AActor* ToActor);
};
