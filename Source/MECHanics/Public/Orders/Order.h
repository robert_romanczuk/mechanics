// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "Order.generated.h"

UENUM(Blueprintable)
enum class EOrderTypes : uint8
{
	MoveToActor,
	Fire
};

/**
 * 
 */
UCLASS()
class MECHANICS_API UOrder : public UObject
{
	GENERATED_BODY()

private:
	EOrderTypes Type;

public:

	UPROPERTY(EditAnywhere, Category = "Order Parameters")
	AActor* FromActor;

	UPROPERTY(EditAnywhere, Category = "Order Parameters")
	AActor* ToActor;

	void MoveOrder(EOrderTypes Type, AActor* FromActor, AActor* ToActor);	
	EOrderTypes GetType();

	//UFUNCTION(BlueprintCallable, Category = "Actions")
	//void MoveToActor();

	UFUNCTION(BlueprintCallable, Category = "Actions")
		AActor* GetToActor();

	UFUNCTION(BlueprintCallable, Category = "Actions")
		AActor* GetFromActor();
};
