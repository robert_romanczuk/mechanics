// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "../Exceptions/AlreadyInSetException.h"
#include "Order.h"
#include "OrdersSet.generated.h"

/**
 * 
 */
UCLASS()
class MECHANICS_API UOrdersSet : public UObject
{
	GENERATED_BODY()
	
private:
	TSet<UOrder*> Orders;

public:
	UFUNCTION(BlueprintCallable, Category = "Orders")
		void AddOrder(UOrder* Order);

	UFUNCTION(BlueprintCallable, Category = "Orders")
		TArray<UOrder*> GetAll();
	
	
};
