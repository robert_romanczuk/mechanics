// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Order.h"
#include "Orderable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI, Blueprintable)
class UOrderable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class MECHANICS_API IOrderable
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	//virtual void Move(UOrdersSet* Orders, const AActor* MoveToActor) = 0;
	
};
