A VERY early version of engineering thesis project in Unreal Engine 4.

Proceed with CAUTION!

MECHanics v0.6

v0.6

* game win and lose logic

v0.5

* player turn improvements
* attack distance
* AP
* resetting parameters on AI End Turn

v0.4

* main menu
* main game camera

v0.3

* very basic AI
* status bar with unit name and HP

v0.2

* pathfiding
* unit movement
* path highlighting